Ansible OpenVPN Client Role
=========

An Ansible role to install and configure an OpenVPN client connection.

Requirements
------------

None

Role Variables
--------------

The following variables are available:

* `openvpn_server` - hostname or IP address of the OpenVPN server
* `openvpn_port` - port to connect on
* `openvpn_extra_opts` - extra client configuration options
* `openvpn_p12_file` - local p12 file to copy
* `openvpn_tls_key_file` - local TLS key file to copy

Dependencies
------------

None.

Example Playbook
----------------

User the role as follows:

```yaml
  - hosts: servers
    roles:
      - role: openvpn_client
```

License
-------

AGPLv3

Author Information
------------------

Rob Connolly [https://webworxshop.com](https://webworxshop.com)
